const getHash = () =>
  window.location.hash.toLocaleLowerCase().split("/").slice(1) || ["/"];
export default getHash;
