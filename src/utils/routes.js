import getHash from "@utils/getHash";
import Welcome from "@templates/welcome";
//import formAPI from "@utils/formAPI";
const Routes = () => {
  const body = null || document.querySelector("body");
  body.classList.add("dark:bg-zinc-800");
  body.classList.add("dark:text-slate-200");
  getFetch();
  let hash = getHash();
  console.log("hash lenght", hash);
  // Home (without hash)
  if (hash.length === 0) {
    if (
      localStorage.getItem("host") === null &&
      localStorage.getItem("apikey") === null &&
      localStorage.getItem("authorizacion") === null
    ) {
      // If no data in localStorage then show welcome
      //body.innerHTML = Welcome();
      //formAPI();
    }
  } else if (hash[0] === "") {
  }
};
export default Routes;
function getFetch() {
  console.log("initialing fetch");
  let source = "https://api19.sapsf.com/odata/v2/";
  let heads = new Headers();
  heads.append("Accept", "application/json");
  heads.append(
    "Authorization",
    "Basic WENQQUFMQk9UQGV4cGVyaWVuY2k6WGNhcmV0LjIwMjE"
  );
  heads.append("DataServiceVersion", "2.0");
  let requestOptions = {
    method: "GET",
    headers: heads,
    mode: "cors",
    credentials: "include",
    cache: "reload",
  };
  fetch(source, requestOptions)
    .then((response) => response.text())
    .then(console.log)
    .catch(console.error);
}
