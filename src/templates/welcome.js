const Welcome = () => {
  const view = `
  <div class="flex justify-center items-center py-12">
    <div class="flex-auto m-8 p-8 max-w-screen-md rounded dark:bg-gray-800 shadow shadow-gray-700">
      <h2 class="text-2xl">Welcome</h2>
      <p>This app use Success Factors API to get information from your organization. <br>Please, check <a
          href="https://help.sap.com/docs/SAP_SUCCESSFACTORS_PLATFORM/d599f15995d348a1b45ba5603e2aba9b/03e1fc3791684367a6a76a614a2916de.html">here</a>.</p>
      <form class="py-5 overflow-hidden" action="#" method="post">
        <div class="grow p-1">
          <label for="host" class="block text-sm font-medium dark:text-gray-50">Host:</label>
          <input id="host" name="host" type="url" placeholder="https://<your-org>.sapsf.com/odata/v2/" required
            class="block w-full shadow-sm rounded focus:border-sky-600 focus:ring-white dark:text-black h-9 p-2">
        </div>
        <div class="grow p-1">
          <label for="apikey" class="block text-sm font-medium dark:text-gray-50">Apikey:</label>
          <input id="apikey" name="apikey" type="text" placeholder="<your-apikey>" required
            class="block w-full shadow-sm rounded focus:border-sky-600 focus:ring-white dark:text-black h-9 p-2">
        </div>
        <div class="grow p-1">
          <label for="authorizacion" class="block text-sm font-medium dark:text-gray-50">Authorizacion:</label>
          <input id="authorizacion" name="authorizacion" type="text" placeholder="Basic <your-authorization>" required
            class="block w-full shadow-sm rounded focus:border-sky-600 focus:ring-white dark:text-black h-9 p-2">
        </div>
        <div class="grow p-1 my-4">
          <button type="submit"
            class="w-full justify-center py-3 font-medium rounded-md text-white bg-sky-600 hover:bg-sky-700 focus:ring">Save</button>
        </div>
      </form>
    </div>
  </div>
  `;
  return view;
};
export default Welcome;
