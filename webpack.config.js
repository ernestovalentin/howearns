const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
/** @type {import('webpack').Configuration} */
const config = {
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js",
  },
  resolve: {
    alias: {
      "@styles": path.resolve(__dirname, "src/styles/"),
      "@utils": path.resolve(__dirname, "src/utils/"),
      "@templates": path.resolve(__dirname, "src/templates/"),
    },
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: "babel-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              importLoaders: 1,
            },
          },
          "postcss-loader",
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "How Earns",
    }),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en/),
    new MiniCssExtractPlugin(),
  ],
};

module.exports = (env, arg) => {
  if (arg.mode === "production") {
    config.output.publicPath = env.url ? env.url : "/";
    config.output.clean = true;
  } else if (arg.mode === "development") {
    config.output.filename = "[name].[contenthash].js";
    config.output.assetModuleFilename = "[name][hash][ext]";
    config.devServer = {
      liveReload: true,
      open: true,
      allowedHosts: "all",
      watchFiles: ["src/**/*html", "src/**/*.js", "src/**/*.css"],
    };
  }
  console.log("Environment: ", env);

  return config;
};
